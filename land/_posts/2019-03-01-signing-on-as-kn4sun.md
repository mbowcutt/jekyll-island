---
layout: page
title: "Signing on as KN4SUN"
categories: land
tags: [radio, amateur, ham, license]
---

My senior project involves amateur radio, so I went and got my Technician's license! I think my callsign is rather catchy. Look me up on [QRZ.com](https://www.qrz.com/db/KN4SUN)! Special thanks to my mentor [David Kazdan, AD8Y](https://www.qrz.com/db/AD8Y), and the [Case Amateur Radio Club, W8EDU](https://www.qrz.com/db/W8EDU) for their support.
